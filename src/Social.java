/*
 * Download and install the latest JDK from http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html
 * 
 * CD into the directory where this file is stored.
 * 
 * Run the following commands:
 * systemprompt> <path to jdk bin folder>/javac Social.java
 * systemprompt> <path to jdk bin folder>/java Social
 * 
 * Command summary:
 * <username> : show the <username>'s message list in chronological order
 * <username> -> <message> : add <message> to the <username>'s message list
 * <username> follows <another_username> : add <another_username> to the <username>'s list of followed users
 * <username> wall : show messages from <username> along with all messages from followed users, in reverse chronological order
 * exit : exit the program
 * 
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.TreeMap;

/**
 *
 */
public class Social {

    /**
     * Holds a message, along with a timestamp and a reference to the user
     * 
     */
    private class Message implements Comparable<Message> {
        public final Date timestamp;
        public final String message;
        public final User owner;

        public Message(Date timestamp, String message, User owner) {
            this.timestamp = timestamp;
            this.message = message;
            this.owner = owner;
        }

        @Override
        public int compareTo(Message other) {
            return timestamp.compareTo(other.timestamp);
        }

        public String getAgeStr() {
            // difference in seconds
            long diff = (new Date().getTime() - timestamp.getTime()) / 1000;
            if (diff < 60) {
                return ("" + diff + " seconds ago");
            } else if (diff < 3600) {
                return ("" + diff / 60 + " minutes ago");
            } else if (diff < 86400) {
                return ("" + diff / 3600 + " hours ago");
            } else {
                return ("" + diff / 86400 + " days ago");
            } // can do months and years too
        }
    }

    /**
     * User record, holds user details, other users that this user is following,
     * and the user's messages
     * 
     */
    private class User {
        public String name;
     // a map of followed users
        private TreeMap<String, User> following = new TreeMap<String, User>();
     // an array of messages
        private ArrayList<Message> messages = new ArrayList<Message>();

        public User(String name) {
            this.name = name;
        }

        public void follow(User user) {
            following.put(user.name, user);
        }

        public void addMessage(String message) {
            messages.add(new Message(new Date(), message, this));
        }

        public Message[] getMessages() {
            return messages.toArray(new Message[messages.size()]);
        }

        public Message[] getWall() {

            ArrayList<Message> wall = new ArrayList<Message>();

            Iterator<String> fi = following.keySet().iterator();

            // add followed users' messages to the wall
            while (fi.hasNext()) {
                User u = following.get(fi.next());
                wall.addAll(u.messages);
            }

            // add own messages to the wall
            wall.addAll(messages);

            // sort messages by date
            Collections.sort(wall);
            // show most recent messages on top
            Collections.reverse(wall);

            return wall.toArray(new Message[wall.size()]);
        }
    }

    // input command parser
    private Pattern inputFormat = Pattern.compile("(\\S+)(?:\\s+(wall|follows|\\-\\>)\\s*(.*))?");

    // list of all users in the system
    private TreeMap<String, User> users = new TreeMap<String, User>();

    public static void main(String[] args) throws IOException {
        (new Social()).run();
    }

    private void run() throws IOException {

        // set up the command line
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String input = "";

        while (true) {

            System.out.print("> ");
            System.out.flush();

            // read input
            input = in.readLine();

            Matcher m = inputFormat.matcher(input);

            // parse input
            if (m.matches()) {
                String username = m.group(1);

                if (username.equals("exit")) {
                    break;
                }

                // create a new user if one does not exist yet
                if (!users.containsKey(username)) {
                    users.put(username, new User(username));
                }

                User u = users.get(username);

                if (m.group(2) == null) {
                    // only the user name is specified, print user's messages

                    System.out.println(/* "begin " + username + "'s messages" */);

                    for (Message msg : u.getMessages()) {
                        System.out.println(msg.message + " (" + msg.getAgeStr() + ")");
                    }

                    System.out.println(/* "end " + username + "'s messages" */);

                } else {

                    if (m.group(2).equals("wall")) {

                        System.out.println(/* "begin " + username + "'s wall" */);

                        for (Message msg : u.getWall()) {
                            System.out.println(msg.owner.name + " - " + msg.message + " (" + msg.getAgeStr() + ")");
                        }

                        System.out.println(/* "end " + username + "'s wall" */);

                    } else if (m.group(2).equals("follows")) {

                        String followed_username = m.group(3);

                        // create the followed user if one does not exist
                        if (!users.containsKey(followed_username)) {
                            users.put(followed_username, new User(followed_username));
                        }

                        u.follow(users.get(followed_username));

                        System.out.println(username + " is now following " + followed_username);

                    } else if (m.group(2).equals("->")) {

                        u.addMessage(m.group(3));

                    }
                }
            } // end of m.matches, can add an else for invalid commands here
        }
    }
}
